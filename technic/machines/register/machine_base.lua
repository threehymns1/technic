local S = technic.getter

local fs_helpers = pipeworks.fs_helpers
local tube_entry = "^pipeworks_tube_connection_metallic.png"

local tube = {
	insert_object = function(pos, node, stack, direction)
		local meta = minetest.get_meta(pos)
		local inv = meta:get_inventory()
		return inv:add_item("src", stack)
	end,
	can_insert = function(pos, node, stack, direction)
		local meta = minetest.get_meta(pos)
		local inv = meta:get_inventory()
		if meta:get_int("splitstacks") == 1 then
			stack = stack:peek_item(1)
		end
		return inv:room_for_item("src", stack)
	end,
	connect_sides = {left = 1, right = 1, back = 1, top = 1, bottom = 1},
}

local connect_default = {"bottom", "back", "left", "right"}

local function round(v)
	return math.floor(v + 0.5)
end

function technic.register_base_machine(data)
	local typename = data.typename
	local input_size = technic.recipes[typename].input_size
	local machine_name = data.machine_name
	local machine_desc = data.machine_desc
	local tier = data.tier
	local ltier = string.lower(tier)

	--NEW: Addon support
	data.modname = data.modname or minetest.get_current_modname()

	assert(type(data.demand) == "table")
	if (tier == "HV") then
		assert(#(data.demand) == 3)
	else
		assert(#(data.demand) == 1)
	end

	local groups = {cracky = 2, technic_machine = 1, ["technic_"..ltier] = 1}
	if data.tube then
		groups.tubedevice = 1
		groups.tubedevice_receiver = 1
	end
	local active_groups = {not_in_creative_inventory = 1}
	for k, v in pairs(groups) do active_groups[k] = v end

	local formspec =
		"invsize[8,9;]"..
		"list[current_name;src;"
			..(4-math.min(input_size,4))..",1;"
			..math.min(input_size,4)..","
			..math.ceil(input_size/4)..";]"..
		"list[current_name;dst;5,1;3,2;]"..
		"list[current_player;main;0,5;8,4;]"..
		"label[0,0;"..machine_desc:format(tier).."]"..
		"listring[current_name;dst]"..
		"listring[current_player;main]"..
		"listring[current_name;src]"..
		"listring[current_player;main]"
	if data.upgrade then
		formspec = formspec..
			"list[current_name;upgrade1;1,3;1,1;]"..
			"list[current_name;upgrade2;2,3;1,1;]"..
			"tooltip[0,0;0,0;GCLU_BEGIN]tooltip[0,0;0,0;GCLU_END]".. -- hidden tooltip use as a regex replace anchor for updates
			"label[1,4;"..S("Upgrade Slots").."]"..
			"listring[current_name;upgrade1]"..
			"listring[current_player;main]"..
			"listring[current_name;upgrade2]"..
			"listring[current_player;main]"..
			"tooltip[0,0;0,0;end]" -- hidden use as a regex replace anchor for updates
	end

	function update_formspec(meta,form_buttons)
		local formspec_meta = meta:get_string("formspec")
		local form_buttons = form_buttons or string.gsub(formspec_meta,".*tooltip.0,0;0,0;end.","")
		formspec_meta = string.gsub(formspec_meta,"tooltip.0,0;0,0;end.*","").."tooltip[0,0;0,0;end]"

		if meta:get_int("IS_GCLU") == 1 then
			local replacement = ""..
				"tooltip[0,0;0,0;GCLU_BEGIN]"..
				"image[4.6,3.1;1,1;technic_control_logic_unit_gold.png]"..
				"label[3.7,3.1;"..S("GCLU\nSettings").."]"..
				"field[5.8,3.45;1.1,1;percent;;${GCLU_Percent}]"..
				"field[6.8,3.45;1.1,1;wait;;${GCLU_Wait}]"..
				"tooltip[5.5,2.95;1,1;"..S("For the Gold Control Logic Unit ONLY")..":"..
				"\n"..
					"\n\t"..S("The percent of the max stack size to reach before sending the stack")..""..
					"\n\t"..S("between").." (0,100%)]"..
				"tooltip[6.5,2.95;1,1;"..S("For the Gold Control Logic Unit ONLY")..":"..
				"\n"..
					"\n\t"..S("The max time (second) to wait before sending the stack")..""..
					"\n\t"..S("between").." (2,3600s),"..
					"\n\t "..S("'0' means no time-checking: be carefull if a stack never reach the percent, it will stay in the machine").."!"..
				"\n"..
					"\n\t"..S("EXAMPLE")..":"..
					"\n\t\t "..S("if a stack never reachs the percent after x seconds, the stack is sent")..". ]"..
				"label[5.55,2.9;"..S("Percent").."]"..
				"label[6.55,2.9;"..S("Time (s)").."]"..
				"tooltip[0,0;0,0;GCLU_END]"
			meta:set_string("formspec", string.gsub(formspec_meta, "tooltip.0,0;0,0;GCLU_BEGIN.tooltip.0,0;0,0;GCLU_END.", replacement)..form_buttons)

		else
			meta:set_string("formspec", string.gsub(formspec_meta, "tooltip.0,0;0,0;GCLU_BEGIN.*tooltip.0,0;0,0;GCLU_END.", "tooltip[0,0;0,0;GCLU_BEGIN]tooltip[0,0;0,0;GCLU_END]")..form_buttons)
		end
	end

	local run = function(pos, node)
		local meta     = minetest.get_meta(pos)
		local inv      = meta:get_inventory()
		local eu_input = meta:get_int(tier.."_EU_input")

		local machine_desc_tier = machine_desc:format(tier)
		local machine_node      = data.modname..":"..ltier.."_"..machine_name
		local machine_demand    = data.demand

		-- Setup meta data if it does not exist.
		if not eu_input then
			meta:set_int(tier.."_EU_demand", machine_demand[1])
			meta:set_int(tier.."_EU_input", 0)
			return
		end

		local EU_upgrade, tube_upgrade = 0, 0
		if data.upgrade then
			EU_upgrade, tube_upgrade = technic.handle_machine_upgrades(meta)
		end
		if data.tube then
			technic.handle_machine_pipeworks(pos, tube_upgrade)
		end

		if type(machine_demand) ~= "table" or not machine_demand[EU_upgrade+1] then
			print("broken demand metadata in machine at "..pos.x..","..pos.y..","..pos.z)
			return
		end
		local powered = eu_input >= machine_demand[EU_upgrade+1]
		if powered then
			meta:set_int("src_time", meta:get_int("src_time") + round(data.speed*10))
		end
		while true do
			local result = technic.get_recipe(typename, inv:get_list("src"))

			-- If type is cooking, also try technic-specific alias, "heating":
			if not result and typename == "cooking" then
				result = technic.get_recipe("heating", inv:get_list("src"))
			end

			if not result then
				technic.swap_node(pos, machine_node)
				meta:set_string("infotext", S("%s Idle"):format(machine_desc_tier))
				meta:set_int(tier.."_EU_demand", 0)
				meta:set_int("src_time", 0)
				return
			end
			meta:set_int(tier.."_EU_demand", machine_demand[EU_upgrade+1])
			technic.swap_node(pos, machine_node.."_active")
			meta:set_string("infotext", S("%s Active"):format(machine_desc_tier))
			if meta:get_int("src_time") < round(result.time*10) then
				if not powered then
					technic.swap_node(pos, machine_node)
					meta:set_string("infotext", S("%s Unpowered"):format(machine_desc_tier))
				end
				return
			end
			local output = result.output
			if type(output) ~= "table" then output = { output } end
			local output_stacks = {}
			local rnd_seed -- seed and flag to check that rnd functions were used and seed updates need to be dealt with
			for _, o in ipairs(output) do
				local name = ItemStack(o):get_name()
				if name:sub(1, 4) == "rnd:" then
					if not rnd_seed then
						-- ensure rnd_seed metadata is initialized and not otherwise (predictably) read as 0
						if not meta:contains("rnd_seed") then
							meta:set_float("rnd_seed", minetest.get_us_time() + math.random())
							meta:mark_as_private("rnd_seed")
						end

						-- obtain the seed that the item should be derived from
						rnd_seed = meta:get_float("rnd_seed")
						math.randomseed(rnd_seed)
					end
					for _,v in ipairs(resolve_rnd_recursive(name)) do
						table.insert(output_stacks, v)
					end
				else
					table.insert(output_stacks, ItemStack(o))
				end
			end
			local room_for_output = false
			inv:set_size("dst", inv:get_size("dst"))
			inv:set_list("dst", inv:get_list("dst"))
			for _, o in ipairs(output_stacks) do
				if inv:room_for_item("dst", o) and inv:room_for_item("dst", "air 99") or machine_desc:find("Reducer") and inv:room_for_item("dst", o) then
					room_for_output = true
				end
			end

			if room_for_output then
				for _, o in ipairs(output_stacks) do
					inv:add_item("dst", o)
				end

				if rnd_seed then
					-- overwrite the seed to use for the item to be generated next
					-- the current one is therefore consumed
					-- (just hard to predict junk for lack of proper hash/crypto functions)
					meta:set_float("rnd_seed", minetest.get_us_time() + math.random())
				end
			end

			if rnd_seed then
				-- set seed back to unrelated junk
				math.randomseed(minetest.get_us_time())
			end

			if not room_for_output then
				-- rnd_seed is not updated, so the same one is used again next time

				technic.swap_node(pos, machine_node)
				meta:set_string("infotext", S("%s Stalled, free up output slots!"):format(machine_desc_tier))
				meta:set_int(tier.."_EU_demand", 0)
				meta:set_int("src_time", round(result.time*10))
				return
			end
			meta:set_int("src_time", meta:get_int("src_time") - round(result.time*10))
			inv:set_list("src", result.new_input)
			inv:set_list("dst", inv:get_list("dst"))
		end
	end

	local tentry = tube_entry
	if ltier == "lv" then
		tentry = nil
	end

	local tentries = {}
	if data.tube_sides == nil then -- all possible sides are drawn with tube port by default
		for i=1, 5 do
			tentries[i] = tentry
		end
	else
		for i=1, 5 do
			if data.tube_sides[i] then
				tentries[i] = tentry
			end
		end
	end

	data.tiles = data.tiles or {}
	-- data.tiles can redefine suffixes used for textures
	data.tiles[1] = data.tiles[1] or "_top"
	data.tiles[2] = data.tiles[2] or "_bottom"
	data.tiles[3] = data.tiles[3] or "_side"
	data.tiles[4] = data.tiles[4] or "_side"
	data.tiles[5] = data.tiles[5] or "_side"
	data.tiles[6] = data.tiles[6] or "_front"

	minetest.register_node(data.modname ..":"..ltier.."_"..machine_name, {
		description = machine_desc:format(tier),
		drawtype = "nodebox",
		node_box = data.node_box or {
			type = "fixed",
			fixed = {
				{-1/2, -1/2, -1/2, 1/2, 1/2, 1/2}
			},
		},
		selection_box = data.selection_box or {
			type = "fixed",
			fixed = {
				{-1/2, -1/2, -1/2, 1/2, 1/2, 1/2},
			},
		},
		tiles = {
			data.modname.."_"..ltier.."_"..machine_name..data.tiles[1]..".png"..(tentries[1] or ""),
			data.modname.."_"..ltier.."_"..machine_name..data.tiles[2]..".png"..(tentries[2] or ""),
			data.modname.."_"..ltier.."_"..machine_name..data.tiles[3]..".png"..(tentries[3] or ""),
			data.modname.."_"..ltier.."_"..machine_name..data.tiles[4]..".png"..(tentries[4] or ""),
			data.modname.."_"..ltier.."_"..machine_name..data.tiles[5]..".png"..(tentries[5] or ""),
			data.modname.."_"..ltier.."_"..machine_name..data.tiles[6]..".png"
		},
		paramtype = "light",
		paramtype2 = "facedir",
		groups = groups,
		tube = data.tube and tube or nil,
		connect_sides = data.connect_sides or connect_default,
		legacy_facedir_simple = true,
		sounds = default.node_sound_wood_defaults(),
		on_construct = function(pos)
			local node = minetest.get_node(pos)
			local meta = minetest.get_meta(pos)

			local form_buttons = ""
			if not string.find(node.name, ":lv_") then
				form_buttons = fs_helpers.cycling_button(
					meta,
					pipeworks.button_base,
					"splitstacks",
					{
						pipeworks.button_off,
						pipeworks.button_on
					}
				)..pipeworks.button_label
				
			end

			meta:set_string("infotext", machine_desc:format(tier))
			meta:set_int("tube_time",  0)
			meta:set_string("formspec", formspec..form_buttons)
			local inv = meta:get_inventory()
			inv:set_size("src", input_size)
			inv:set_size("dst", 6)
			inv:set_size("upgrade1", 1)
			inv:set_size("upgrade2", 1)
		end,
		can_dig = technic.machine_can_dig,
		allow_metadata_inventory_put = technic.machine_inventory_put,
		allow_metadata_inventory_take = technic.machine_inventory_take,
		allow_metadata_inventory_move = technic.machine_inventory_move,
		technic_run = run,
		after_place_node = data.tube and function(pos) 
			 pipeworks.after_place(pos)
			 local meta = minetest.get_meta(pos)
		end,
		after_dig_node = technic.machine_after_dig_node,
		on_receive_fields = function(pos, formname, fields, sender)
			local node = minetest.get_node(pos)
			if not pipeworks.may_configure(pos, sender) then return end
			fs_helpers.on_receive_fields(pos, fields)
			local meta = minetest.get_meta(pos)
			local form_buttons = ""
			if not string.find(node.name, ":lv_") then
				form_buttons = fs_helpers.cycling_button(
					meta,
					pipeworks.button_base,
					"splitstacks",
					{
						pipeworks.button_off,
						pipeworks.button_on
					}
				)..pipeworks.button_label
			end
			
			local percent = tonumber(fields.percent)
			local wait =  tonumber(fields.wait)

			if percent then
				if percent < 0 then percent = 0 
				elseif percent > 100 then percent = 100 end
				meta:set_int("GCLU_Percent",percent)
			end

			if wait then
				if wait == 0 then
					-- do nothing
				elseif wait < 2 then wait = 2 
				elseif wait > 3600 then wait = 3600 end
				meta:set_int("GCLU_Wait",wait)
			end
			
			update_formspec(meta,form_buttons)
		end,
	})

	minetest.register_node(data.modname..":"..ltier.."_"..machine_name.."_active",{
		description = machine_desc:format(tier),
		drawtype = "nodebox",
		node_box = data.node_box or {
			type = "fixed",
			fixed = {
				{-1/2, -1/2, -1/2, 1/2, 1/2, 1/2}
			},
		},
		selection_box = data.selection_box or {
			type = "fixed",
			fixed = {
				{-1/2, -1/2, -1/2, 1/2, 1/2, 1/2},
			},
		},
		tiles = {
			data.modname.."_"..ltier.."_"..machine_name..data.tiles[1]..".png"..(tentries[1] or ""),
			data.modname.."_"..ltier.."_"..machine_name..data.tiles[2]..".png"..(tentries[2] or ""),
			data.modname.."_"..ltier.."_"..machine_name..data.tiles[3]..".png"..(tentries[3] or ""),
			data.modname.."_"..ltier.."_"..machine_name..data.tiles[4]..".png"..(tentries[4] or ""),
			data.modname.."_"..ltier.."_"..machine_name..data.tiles[5]..".png"..(tentries[5] or ""),
			data.modname.."_"..ltier.."_"..machine_name..data.tiles[6].."_active.png"
		},
		paramtype = "light",
		paramtype2 = "facedir",
		drop = data.modname..":"..ltier.."_"..machine_name,
		groups = active_groups,
		connect_sides = data.connect_sides or connect_default,
		legacy_facedir_simple = true,
		sounds = default.node_sound_wood_defaults(),
		tube = data.tube and tube or nil,
		can_dig = technic.machine_can_dig,
		allow_metadata_inventory_put = technic.machine_inventory_put,
		allow_metadata_inventory_take = technic.machine_inventory_take,
		allow_metadata_inventory_move = technic.machine_inventory_move,
		technic_run = run,
		technic_disabled_machine_name = data.modname..":"..ltier.."_"..machine_name,
		on_receive_fields = function(pos, formname, fields, sender)
			local node = minetest.get_node(pos)
			if not pipeworks.may_configure(pos, sender) then return end
			fs_helpers.on_receive_fields(pos, fields)
			local meta = minetest.get_meta(pos)
			local form_buttons = ""
			if not string.find(node.name, ":lv_") then
				form_buttons = fs_helpers.cycling_button(
					meta,
					pipeworks.button_base,
					"splitstacks",
					{
						pipeworks.button_off,
						pipeworks.button_on
					}
				)..pipeworks.button_label
			end
			update_formspec(meta,form_buttons)

			local percent = tonumber(fields.percent)
			local wait =  tonumber(fields.wait)

			if percent then
				if percent < 0 then percent = 0 
				elseif percent > 100 then percent = 100 end
				meta:set_int("GCLU_Percent",percent)
			end

			if wait then
				if wait == 0 then
					-- do nothing
				elseif wait < 2 then wait = 2 
				elseif wait > 3600 then wait = 3600 end
				meta:set_int("GCLU_Wait",wait)
			end
		end,
	})

	technic.register_machine(tier, data.modname..":"..ltier.."_"..machine_name,            technic.receiver)
	technic.register_machine(tier, data.modname..":"..ltier.."_"..machine_name.."_active", technic.receiver)
end -- End registration


